import React, { useRef, useState, useEffect } from "react"
import { View, StyleSheet, Text, TouchableOpacity, Image, Platform,Dimensions } from "react-native"
import Permissions from 'react-native-permissions';
import PDFScanner from "@woonivers/react-native-document-scanner"
import RNImageToPdf from 'react-native-image-to-pdf';


export default function App() {
  const pdfScannerElement = useRef(null)
  const [data, setData] = useState({})
  const [allowed, setAllowed] = useState(false)
  const [storageReadAllowed,setStorageReadAllowed]=useState(false)
  const [storageWriteAllowed,setStorageWriteAllowed]=useState(false)
   myAsyncPDFFunction = async (images,name) => {
    try {
      const windowWidth = Dimensions.get('window').width;
      const windowHeight = Dimensions.get('window').height;
      console.log(windowWidth+"  "+windowHeight)
      const options = {
        imagePaths: [images.split(':')[1]],
        name: name,
      };
      console.log(options)
      const pdf = await RNImageToPdf.createPDFbyImages(options);
      console.log(pdf.filePath);
    } catch(e) {
      console.log(e);
    }
  }
  useEffect(() => {
    async function requestCamera() {
      const result = await Permissions.request(Platform.OS === "android" ? "android.permission.CAMERA" : "ios.permission.CAMERA")
      if (result === "granted") setAllowed(true)
    }
    async function requestStorageRead() {
      const result = await Permissions.request(Platform.OS === "android" ? "android.permission.READ_EXTERNAL_STORAGE" : "ios.permission.CAMERA")
      if (result === "granted") setStorageReadAllowed(true)
    }
    async function requestStorageWrite() {
      const result = await Permissions.request(Platform.OS === "android" ? "android.permission.WRITE_EXTERNAL_STORAGE" : "ios.permission.CAMERA")
      if (result === "granted") setStorageWriteAllowed(true)
    }
    requestStorageRead()
    requestStorageWrite()
    requestCamera()
  }, [])
  function handleOnPressRetry() {
    setData({})
  }
  function handleOnPress() {
    pdfScannerElement.current.capture()
  }
  if (!(allowed&&storageWriteAllowed&&storageReadAllowed)) {
    console.log("You must accept camera permission")
    return (<View style={styles.permissions}>
      <Text>You must accept camera permission</Text>
    </View>)
  }
  if (data.croppedImage) {
    console.log("data", data)
    return (
      <React.Fragment>
        <Image source={{ uri: data.croppedImage }} style={styles.preview} />
        <TouchableOpacity onPress={handleOnPressRetry} style={styles.buttonRetry}>
          <Text style={styles.buttonText}>Retry</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>myAsyncPDFFunction(data.croppedImage,"MyPDFFile.pdf")} style={styles.buttonSave}>
          <Text style={styles.buttonText}>Save</Text>
        </TouchableOpacity>
      </React.Fragment>
    )
  }
  return (
    <React.Fragment>
      <PDFScanner
        ref={pdfScannerElement}
        style={styles.scanner}
        onPictureTaken={setData}
        overlayColor="rgba(255,130,0, 0.7)"
        enableTorch={false}
        quality={0.5}
        detectionCountBeforeCapture={5}
        detectionRefreshRateInMS={50}
      />
      <TouchableOpacity onPress={handleOnPress} style={styles.button}>
        <Text style={styles.buttonText}>Take picture</Text>
      </TouchableOpacity>
    </React.Fragment>
  )
}

const styles = StyleSheet.create({
  scanner: {
    flex: 1,
    aspectRatio: undefined
  },
  buttonRetry: {
    alignSelf:"flex-start",
    position: "absolute",
    bottom: 32,
  },
  buttonSave: {
    alignSelf:"flex-end",
    position: "absolute",
    bottom: 32,
  },
  button: {
    alignSelf:"center",
    position: "absolute",
    bottom: 32,
  },
  buttonText: {
    backgroundColor: "rgba(245, 252, 255, 0.7)",
    fontSize: 32,
  },
  preview: {
    flex: 1,
    width: "100%",
    resizeMode: "cover",
  },
  permissions: {
    flex:1,
    justifyContent: "center",
    alignItems: "center"
  }
})